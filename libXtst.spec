Summary:        X.Org X11 libXtst runtime library
Name:           libXtst
Version:        1.2.5
Release:        1
License:        MIT
URL:            https://www.x.org
Source0:        https://www.x.org/releases/individual/lib/libXtst-%{version}.tar.xz

Requires: libX11 >= 1.6
BuildRequires: make gcc
BuildRequires: pkgconfig(inputproto)
BuildRequires: pkgconfig(recordproto) >= 1.13.99.1
BuildRequires: pkgconfig(x11) >= 1.6
BuildRequires: pkgconfig(xext) >= 1.0.99.4
BuildRequires: pkgconfig(xextproto) >= 7.0.99.3
BuildRequires: pkgconfig(xi)
BuildRequires: pkgconfig(xorg-macros) >= 1.12

%description
X.Org X11 libXtst runtime library

%package        devel
Summary:        Development package for ${name}
Requires:       %{name} = %{version}-%{release}

%description    devel
The %{name}-devel package contains libraries and header files for %{name}.

%package_help

%prep
%autosetup -n %{name}-%{version} -p1

%build
%configure --disable-static
%make_build

%install
%make_install
%delete_la

rm -fr %{buildroot}%{_docdir}

%files
%license COPYING
%{_libdir}/*.so.*

%files devel
%{_includedir}/X11/extensions/*.h
%{_libdir}/*.so
%{_libdir}/pkgconfig/*.pc

%files help
%{_mandir}/man3/XTest*.3*

%changelog
* Mon Dec 02 2024 Funda Wang <fundawang@yeah.net> - 1.2.5-1
- update to 1.2.5

* Tue Jul 30 2024 lingsheng <lingsheng1@h-partners.com> - 1.2.4-2
- Coverity CID 1373522: Fix memory leak

* Thu Nov 03 2022 zhouwenpei <zhouwenpei1@h-partners.com> - 1.2.4-1
- update 1.2.4

* Wed Oct 26 2022 zhouwenpei <zhouwenpei1@h-partners.com> - 1.2.3-11
- Rebuild for next release

* Sat Oct 19 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.2.3-10
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:change the directory of the license file

* Mon Sep 16 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.2.3-9
- Package init
